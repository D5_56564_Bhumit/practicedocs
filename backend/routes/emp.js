const { query} = require("express");
const express = require("express");
const router = express.Router();
const utils = require("../utils");

router.get("/:name", (request, response) => {
  const { name } = request.params;
  const connection = utils.openConnection();
  const statement = `select * from emp where name='${name}'`;
  connection.query(statement, (error, result) => {
    connection.end();
    if (result.length > 0) {
      response.send(utils.createResult(error, result));
    } else {
      response.send("user not found");
    }
  });
});
router.post("/add", (request, reponse) => {
  const { name, salary, age } = request.body;
  const connection = utils.openConnection();
  const statement = `insert into emp(name,salary,age)
                    values('${name}',${salary},${age})
                    `;
  connection.query(statement, (error, result) => {
    connection.end();
    reponse.send(utils.createResult(error, result));
  });
});

router.put("/update/:name", (request, response) => {
  const { name } = request.params;
  const { salary } = request.body;
  const connection = utils.openConnection();
  const statement = `update  emp set salary='${salary}' where name='${name}'`;
  connection.query(statement, (error, result) => {
    connection.end();
    response.send(utils.createResult(error, result));
  });
});

router.delete("/remove/:name", (request, response) => {
  const { name } = request.params;
  const statement = `
      delete from emp
      where
        name = '${name}'
    `;
  const connection = utils.openConnection();
  connection.query(statement, (error, result) => {
    connection.end();
    console.log(statement);
    response.send(utils.createResult(error, result));
  });
});

module.exports = router;